/**
 * Run generate tasks in entire monorepo
 */

import { $ } from 'execa'

async function run() {
  // Run generate tasks recursively
  await $`pnpm --recursive generate`.pipeStdout?.(process.stdout)
}

run()
