/**
 * Run package verisioning steps
 */

import { $ } from 'execa'
import consola from 'consola'

async function run() {
  consola.start('Update versions')
  await $`pnpm changeset version`
  consola.success('Done')

  consola.start('Write VERSION files')
  await $`pnpm vite-node ./scripts/writeVersionsToFile.ts`
  consola.success('Done')
}

run()
