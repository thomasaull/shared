# Setup

## Gitlab CI

Note: `changesets-gitlab` theoretically supports using a `CI_JOB_TOKEN` but since the `comment` task needs `api` access this is blocked by this issue: https://gitlab.com/groups/gitlab-org/-/epics/3559

1. Create a personal access token
1. Add CI Variable `GITLAB_TOKEN` with the value of the personal access token
1. Add `.npmrc` file with the following contents:

```
@thomasaull-shared:registry=https://gitlab.com/api/v4/projects/48892958/packages/npm/
```

1. Rename packages, use external packages, etc…

## IDE

- Use Takeover mode: https://vuejs.org/guide/typescript/overview.html#takeover-mode

# Misc

## Filter commits from `git blame`

- Update `.git-blame-ignore-revs`
- Add the following to `/git/config`

```
[blame]
	ignoreRevsFile = .git-blame-ignore-revs
```

## htaccess for single page applications

See: https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations

# Some reading material

- https://miyauchi.dev/posts/vite-vue3-typescript/
