---
'@thomasaull-shared/shared': patch
---

📦 Move some packages from dev to regular dependencies
