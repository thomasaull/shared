import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv, mergeConfig } from 'vite'

import { createViteConfig } from '@thomasaull-shared/vite-config'

export const configOverrides = defineConfig((options) => {
  const env = loadEnv(options.mode, process.cwd(), '')

  return {
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      },
    },
  }
})

export default defineConfig((configEnv) =>
  mergeConfig(createViteConfig(configEnv, {}), configOverrides(configEnv))
)
