import {
  defaultConfig,
  createStoriesConfig,
  type StorybookConfig,
} from '@thomasaull-shared/storybook-config/main'

const config: StorybookConfig = {
  ...defaultConfig,
  stories: createStoriesConfig({ storiesPaths: ['../src'] }),
}

export default config
