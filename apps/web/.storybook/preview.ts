import { type Preview, defaultConfig } from '@thomasaull-shared/storybook-config/preview'
import '@/shared'

const preview: Preview = {
  ...defaultConfig,
}

export default preview
