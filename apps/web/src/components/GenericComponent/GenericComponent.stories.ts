import {
  type GenericMeta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import GenericComponent from '@/components/GenericComponent/GenericComponent.vue'
import ExampleComponent from '@/components/ExampleComponent/ExampleComponent.vue'

const meta = {
  ...generateStoryMeta(GenericComponent),

  title: 'GenericComponent',
} satisfies GenericMeta<typeof GenericComponent>

export default meta
type Story = StoryObj<typeof meta>

export const DifferentComponent: Story = {
  ...generateStory({
    component: ExampleComponent,
  }),
}

export const DifferentComponentAndTemplate: Story = {
  ...generateStory({
    components: { GenericComponent, ExampleComponent },
    template: 'Blupp',
  }),
}
