import {
  type Meta,
  type StoryObj,
  generateStoryMeta,
  generateStory,
} from '@thomasaull-shared/storybook'

import ExampleInput from '@/components/ExampleInput/ExampleInput.vue'

const meta = {
  ...generateStoryMeta(ExampleInput),

  title: 'ExampleInput',
  component: ExampleInput,
} satisfies Meta<typeof ExampleInput>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
  ...generateStory(),
}
