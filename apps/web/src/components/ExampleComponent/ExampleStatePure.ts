import { setup } from 'xstate'

export const ExampleStatePure = setup({}).createMachine({
  id: 'ExampleStatePure',
  initial: 'default',

  states: {
    default: {},

    anotherState: {
      initial: 'blupp',

      meta: {
        title: 'Meta for "anotherState"',
      },

      states: {
        bla: {
          initial: 'child',

          states: {
            child: {},
            wild: {},
          },
        },

        blupp: {},
      },
    },
  },
})
