import { fileURLToPath } from 'node:url'
import { mergeConfig, defineConfig, configDefaults } from 'vitest/config'
import viteConfig from './vite.config'

const configOverrides = defineConfig((options) => {
  return {
    test: {
      environment: 'jsdom',
      exclude: [...configDefaults.exclude, 'e2e/*'],
      root: fileURLToPath(new URL('./', import.meta.url)),
    },
  }
})

export default defineConfig((configEnv) => {
  return mergeConfig(viteConfig(configEnv), configOverrides(configEnv))
})
