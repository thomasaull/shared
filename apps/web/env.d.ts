/// <reference types="@thomasaull-shared/vite-config/env.d.ts" />

/**
 * Add env variables as types
 * @see https://vitejs.dev/guide/env-and-mode.html#intellisense-for-typescript
 */
interface ImportMetaEnv {
  readonly VITE_API_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
