# @thomasaull-shared/web

## 1.0.23

### Patch Changes

- Updated dependencies [83c9d69]
  - @thomasaull-shared/composables@2.1.0

## 1.0.22

### Patch Changes

- Updated dependencies [ff539ba]
  - @thomasaull-shared/composables@2.0.3

## 1.0.21

### Patch Changes

- Updated dependencies [be2d5d7]
  - @thomasaull-shared/composables@2.0.2

## 1.0.20

### Patch Changes

- Updated dependencies [527028e]
  - @thomasaull-shared/composables@2.0.1

## 1.0.19

### Patch Changes

- Updated dependencies [ee940c0]
  - @thomasaull-shared/composables@2.0.0

## 1.0.18

### Patch Changes

- Updated dependencies [1b101ac]
  - @thomasaull-shared/composables@1.4.0

## 1.0.17

### Patch Changes

- Updated dependencies [4a79514]
  - @thomasaull-shared/composables@1.3.0

## 1.0.16

### Patch Changes

- Updated dependencies [6775213]
  - @thomasaull-shared/scss@0.1.0

## 1.0.15

### Patch Changes

- Updated dependencies [b60426f]
- Updated dependencies [b5645e3]
  - @thomasaull-shared/composables@1.2.1

## 1.0.14

### Patch Changes

- Updated dependencies [f45e505]
  - @thomasaull-shared/composables@1.2.0

## 1.0.13

### Patch Changes

- Updated dependencies [73f0f8a]
  - @thomasaull-shared/composables@1.1.5

## 1.0.12

### Patch Changes

- Updated dependencies [49f01e5]
  - @thomasaull-shared/composables@1.1.4

## 1.0.11

### Patch Changes

- Updated dependencies [b7f9a50]
  - @thomasaull-shared/composables@1.1.3

## 1.0.10

### Patch Changes

- Updated dependencies [034848b]
  - @thomasaull-shared/composables@1.1.2

## 1.0.9

### Patch Changes

- Updated dependencies [9a649e3]
  - @thomasaull-shared/composables@1.1.1

## 1.0.8

### Patch Changes

- Updated dependencies [f7070dd]
  - @thomasaull-shared/composables@1.1.0

## 1.0.7

### Patch Changes

- Updated dependencies [5ead9d1]
  - @thomasaull-shared/composables@1.0.1

## 1.0.6

### Patch Changes

- Updated dependencies [51f128e]
  - @thomasaull-shared/composables@1.0.0

## 1.0.5

### Patch Changes

- Updated dependencies [ab80f15]
  - @thomasaull-shared/composables@0.0.4

## 1.0.4

### Patch Changes

- Updated dependencies [b6d0dd6]
- Updated dependencies [a173553]
  - @thomasaull-shared/composables@0.0.3

## 1.0.3

### Patch Changes

- Updated dependencies [c830d96]
  - @thomasaull-shared/composables@0.0.2

## 1.0.2

### Patch Changes

- Updated dependencies [b10b249]
  - @thomasaull-shared/scss@0.0.2

## 1.0.1

### Patch Changes

- Updated dependencies [8715cb5]
  - @thomasaull-shared/scss@0.0.1

## 1.0.0

### Major Changes

- 976f86d: Bump version to v1

## 0.0.2

### Patch Changes

- f3e5697: Trigger release of all packages
