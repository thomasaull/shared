/* eslint-env node */

module.exports = {
  extends: [
    'stylelint-config-html',
    'stylelint-config-standard',
    'stylelint-config-standard-scss',
    'stylelint-config-standard-vue',
  ],
  plugins: ['stylelint-selector-bem-pattern'],

  rules: {
    /**
     * Allow scss function names like `mySpecialFunction`
     * instead of only lower-case like `myspecialfunction`
     *
     * @see https://stylelint.io/user-guide/rules/function-name-case/
     */
    'function-name-case': null,

    /**
     * Streamline notation of alpha values
     *
     * @see https://stylelint.io/user-guide/rules/alpha-value-notation/
     */
    'alpha-value-notation': 'number',

    /**
     * Disable this rule since it does not work well with the selector-bem-pattern plugin
     * @see https://github.com/simonsmith/stylelint-selector-bem-pattern/issues/36#issuecomment-562935399
     *
     * An alternative would be to use a custom regex
     * @see https://github.com/simonsmith/stylelint-selector-bem-pattern/issues/36#issuecomment-562935399
     *
     */
    'selector-class-pattern': null,

    /**
     * Turn off enforcement of modern color notation which does not work really well with sass
     * @see https://stylelint.io/user-guide/rules/color-function-notation/
     * @see https://github.com/sass/sass/issues/3036
     */
    'color-function-notation': 'legacy',

    /**
     * Allow values of zero without unit for custom-properties
     *
     * This is sometimes needed, when you want to use a arbitrary value as a custom property
     * which is supposed to be multiplied with different unit-values
     * @example
     * ```
     * --my-custom-property: 2;
     * width: calc(var(--my-custom-property) * 10%) // Multiply with %
     * height: calc(var(--my-custom-property) * 20px) // Multiply with px
     * ```
     *
     * @see https://stylelint.io/user-guide/rules/length-zero-no-unit/
     */
    'length-zero-no-unit': [
      true,
      {
        ignore: ['custom-properties'],
      },
    ],

    /**
     * Allow some keywords to be camelCase, e.g. currentColor
     * @see https://stylelint.io/user-guide/rules/value-keyword-case/
     */
    'value-keyword-case': [
      'lower',
      {
        camelCaseSvgKeywords: true,
      },
    ],

    /**
     * Make @-rules work with scss
     * @see https://github.com/stylelint-scss/stylelint-scss/tree/master/src/rules/at-rule-no-unknown
     */
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,

    /**
     * Make functions work with scss
     * @see https://github.com/stylelint-scss/stylelint-scss/tree/master/src/rules/function-no-unknown
     */
    'function-no-unknown': null,
    'scss/function-no-unknown': [
      true,
      {
        ignoreFunctions: ['v-bind'],
      },
    ],

    /**
     * Don't restrict how $variables are named
     * stylelint-config-standard-scss restricts this to kebap-case
     * @see https://github.com/stylelint-scss/stylelint-config-standard-scss/blob/main/index.js#L46
     * @see https://github.com/stylelint-scss/stylelint-scss/tree/master/src/rules/dollar-variable-pattern
     *
     * @todo: When a naming pattern emerges, we could add a regex for this rule
     */
    'scss/dollar-variable-pattern': null,

    /**
     * Don't restrict how mixins are named
     * stylelint-config-standard-scss restricts this to kebap-case
     * @see https://github.com/stylelint-scss/stylelint-config-standard-scss/blob/main/index.js#L30
     * @see https://github.com/stylelint-scss/stylelint-scss/tree/master/src/rules/at-mixin-pattern
     *
     * @todo: When a naming pattern emerges, we could add a regex for this rule
     */
    'scss/at-mixin-pattern': null,

    /**
     * Don't restrict how functions are named
     * stylelint-config-standard-scss restricts this to kebap-case
     * @see https://github.com/stylelint-scss/stylelint-config-standard-scss/blob/main/index.js#L20
     * @see https://github.com/stylelint-scss/stylelint-scss/tree/master/src/rules/at-function-pattern
     *
     * @todo: When a naming pattern emerges, we could add a regex for this rule
     */
    'scss/at-function-pattern': null,

    /**
     * Don't restrict how custom-properties are named
     * stylelint-config-standard restricts this to kebap-case
     * @see https://github.com/stylelint/stylelint-config-standard/blob/main/index.js#L49
     * @see https://stylelint.io/user-guide/rules/custom-property-pattern/
     *
     * @todo: When a naming pattern emerges, we could add a regex for this rule
     */
    'custom-property-pattern': null,

    /**
     * Don't restrict how @keyframes are named
     * stylelint-config-standard restricts this to kebap-case
     * @see https://github.com/stylelint/stylelint-config-standard/blob/main/index.js#L70
     * @see https://stylelint.io/user-guide/rules/keyframes-name-pattern/
     *
     * @todo: When a naming pattern emerges, we could add a regex for this rule
     */
    'keyframes-name-pattern': null,

    /**
     * Dont restrict how comments are used
     *
     * @see https://github.com/stylelint-scss/stylelint-scss/blob/master/src/rules/double-slash-comment-empty-line-before/README.md
     */
    'scss/double-slash-comment-empty-line-before': null,

    /**
     * Don't restrict if there should be a line or not before a $variable
     *
     * @see https://github.com/stylelint-scss/stylelint-scss/blob/master/src/rules/dollar-variable-empty-line-before/README.md
     */
    'scss/dollar-variable-empty-line-before': null,

    /**
     * Dont force usage of shorthand properties, sometimes the extended syntax is just more readable
     *
     * @see https://stylelint.io/user-guide/rules/declaration-block-no-redundant-longhand-properties/
     */
    'declaration-block-no-redundant-longhand-properties': null,

    'plugin/selector-bem-pattern': {
      implicitComponents: ['**/src/components/**/*'],
      preset: 'suit',
      ignoreSelectors: [
        /^%/, //placeholder selector
        /^#{\$block}/,
        /^svg$/,
        /v-enter-from$/,
        /v-enter-to$/,
        /v-enter-active$/,
        /v-leave-from$/,
        /v-leave-to$/,
        /v-leave-active$/,
        /router-link-active$/,
        /router-link-exact-active$/,
        /^\.App/,
      ],
    },
  },
}
