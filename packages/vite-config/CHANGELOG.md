# @thomasaull-shared/vite-config

## 1.0.1

### Patch Changes

- Updated dependencies [091a6cf]
  - @thomasaull-shared/typescript-config@1.0.1

## 1.0.0

### Major Changes

- 976f86d: Bump version to v1

### Patch Changes

- Updated dependencies [976f86d]
  - @thomasaull-shared/typescript-config@1.0.0

## 0.0.1

### Patch Changes

- f3e5697: Trigger release of all packages
- Updated dependencies [f3e5697]
  - @thomasaull-shared/typescript-config@0.0.1
