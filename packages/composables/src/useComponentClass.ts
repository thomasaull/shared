import { isNil } from 'lodash-es'
import { getCurrentInstance } from 'vue'
import type { Get, RequiredDeep } from 'type-fest'
import { pascalCase } from '@thomasaull-shared/shared/dom'

type BaseOptions = {
  /** For overriding the component name extracted from the vue instance */
  componentName?: string
  /** Pass the props for additional type safety. This is highly recommended if you want to use the `propModifier` function */
  props?: BaseProps
}

type BaseProps = Record<string, unknown>

export function useComponentClass<Options extends BaseOptions>(options?: Options) {
  type PropName = keyof Get<RequiredDeep<typeof options>, 'props'>

  const instance = getCurrentInstance()
  if (!instance) throw new Error('instance is undefined')

  const componentName = options?.componentName ?? instance.type.name ?? instance.type.__name
  if (!componentName) throw new Error('componentName is undefined')

  /**
   * Create a element class
   * @example `MyComponent-myElement`
   */
  function element(name: string) {
    return className({ element: name })
  }

  /**
   * Create a modifier class
   * @example `MyComponent--myModifier`
   * If you want to use a modifier on an element, use `className` instead
   */
  function modifier(modifier: string) {
    return className({ modifier })
  }

  /**
   * Universal method to create a block-element--modifier class
   */
  function className(options: { element?: string; modifier?: string }) {
    if (options.element && options.modifier) {
      return `${componentName}-${options.element}--${options.modifier}`
    }

    if (!options.element && options.modifier) {
      return `${componentName}--${options.modifier}`
    }

    if (options.element) {
      return `${componentName}-${options.element}`
    }

    throw new Error('className should always return a string')
  }

  function createPropModifier(options: { name: PropName; value: string }) {
    return `${String(options.name)}${pascalCase(options.value)}`
  }

  /**
   * Automatically creates a modifier from a prop name and (optional) value
   * Examples how different types are transformed, for a prop named `padding`
   * - `horizontal` → `MyComponent--paddingHorizontal`
   * - `true` → `MyComponent--hasPadding`
   * - `false` → `MyComponent--hasNotPadding`
   *
   * @example
   * ```vue
   * <div :class="[propModifier({ name: 'variant', value: prop.variant })]">
   * ```
   */
  function propModifier(options: {
    /** Providing an element creates a class like `MyComponent-myElement--myModifier` */
    element?: string
    name: PropName
    /** When value is not set, it'll be read from the component props */
    value?: string | undefined | boolean
  }) {
    let value = options.value

    // Try to get the value from the instance.props
    if (isNil(value)) {
      const propValue = getPropValue({ name: options.name })

      // Check if the prop is a string or a boolean
      if (typeof propValue !== 'string' && typeof propValue !== 'boolean') {
        throw new Error(`prop value of "${String(options.name)}" not a string or boolean`)
      }

      value = propValue
    }

    /**
     * Special handling for true | false values
     */
    if (value === true || value === false) {
      const prefix = value ? 'has' : 'hasNot'
      const modifier = `${prefix}${pascalCase(String(options.name))}`

      return className({ element: options.element, modifier })
    }

    return className({
      element: options.element,
      modifier: createPropModifier({
        name: options.name,
        value: value,
      }),
    })
  }

  function getPropValue(options: { name: PropName }) {
    if (!instance) return
    if (typeof options.name !== 'string') return

    const instanceProps = instance.props

    // Check if component has a prop with that name
    if (!(options.name in instanceProps)) {
      throw new Error(`prop "${String(options.name)}" not found`)
    }

    const propValue = instance.props[options.name]

    // Return fallback when the prop value is undefined
    if (isNil(propValue)) return 'none'

    return propValue
  }

  return { element, modifier, root: componentName, className, propModifier }
}
