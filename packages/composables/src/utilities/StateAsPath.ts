/**
 * I have no idea how this works, but apparently it does what it’s supposed to
 * Generated via cloude:
 * @see https://claude.ai/chat/efb5b5cd-3d30-4397-838c-95d8db3e3633
 *
 */
import type { UnionToIntersection } from 'type-fest'

type Join<K, P> = K extends string | number
  ? P extends string | number
    ? `${K}${P extends '' ? '' : '.'}${P}`
    : never
  : never

type LastInUnion<U> =
  UnionToIntersection<U extends any ? () => U : never> extends () => infer R ? R : never

type UnionToTuple<U, Last = LastInUnion<U>> = [U] extends [never]
  ? []
  : [...UnionToTuple<Exclude<U, Last>>, Last]

type StateMachinePaths<T> = T extends string
  ? T
  : T extends object
    ? {
        [K in keyof T]: K extends string | number
          ? T[K] extends string
            ? `${K}` | Join<K, T[K]>
            : `${K}` | Join<K, StateMachinePaths<T[K]>>
          : never
      }[keyof T]
    : never

type FlattenUnion<T> = UnionToTuple<T>[number]

export type StateAsPath<TMachine> = FlattenUnion<StateMachinePaths<TMachine>>

/**
 * Alternative old method, which has the problem that it does not extract all possible state paths
 *
 * Type to extract all possible states to a string union with dot syntax
 * Adapted from: @see https://stackoverflow.com/a/68404823
 * @see https://dev.to/pffigueiredo/typescript-utility-keyof-nested-object-2pa3
 * @see https://github.com/sindresorhus/type-fest/blob/main/source/paths.d.ts
 */
type DotPrefix<T extends string> = T extends '' ? '' : `.${T}`

type DotNestedKeys<T> = (
  T extends object
    ? {
        [K in Exclude<keyof T, symbol>]: `${K}${DotPrefix<DotNestedKeys<T[K]>>}`
      }[Exclude<keyof T, symbol>]
    : T
) extends infer D
  ? Extract<D, string>
  : never
