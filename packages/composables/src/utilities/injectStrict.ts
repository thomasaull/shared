import { inject, type InjectionKey, type Ref } from 'vue'
/**
 * Provide/Inject strict
 *  @see https://logaretm.com/blog/2020-12-23-type-safe-provide-inject/
 */

/**
 * Workaround for bug T | () => T does not work
 * @see https://github.com/microsoft/TypeScript/issues/37663
 */
export function injectStrictWithFactory<T>(key: InjectionKey<T>, fallback?: () => T): T {
  const fallbackValue: T | undefined = fallback ? fallback() : undefined
  return injectStrict(key, fallbackValue)
}

export function injectStrict<T>(key: InjectionKey<T>, fallback?: T): T {
  //  Runtime check to make sure fallback is not a factory function
  if (typeof fallback === 'function') {
    throw new Error(
      'If you want to use a factory function for "injectStrict" please use "injectStrictWithFactory" instead'
    )
  }

  const resolved = inject(key, fallback)

  if (resolved === undefined) {
    throw new Error(`Could not resolve ${key.description}`)
  }

  return resolved
}
