/**
 * Composable for using native html form validation
 * @see https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation#implementing_a_customized_error_message
 */
import { computed, type Ref, ref, isRef, watch } from 'vue'
import type { Entries, Get } from 'type-fest'
import { isNil } from 'lodash-es'
import { useEventListener } from '@vueuse/core'

export type ValidationStateKey = keyof ValidityState | 'notAllowed' | 'callback'
export type ValidationMessages = Partial<Record<ValidationStateKey, string>>

/**
 * Predefined validation messages for different input types
 */
const validationMessagesBase: ValidationMessages = {}
const validationMessagesNumber: ValidationMessages = {}
const validationMessagesEmail: ValidationMessages = {}
const validationMessagesPhone: ValidationMessages = {}
const validationMessagesPassword: ValidationMessages = {}
const validationMessagesSelect: ValidationMessages = {}
const validationMessagesFile: ValidationMessages = {}
const validationMessagesRadio: ValidationMessages = {}

type Type =
  | 'text'
  | 'textarea'
  | 'number'
  | 'tel'
  | 'email'
  | 'password'
  | 'select'
  | 'file'
  | 'radio'
  | 'checkbox'

type Options = {
  elInput: Ref<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement | undefined>
  type?: Ref<Type | undefined> | Type
  validationMessages?: Ref<ValidationMessages | undefined>
  minlength?: Ref<number | undefined>
  pattern?: Ref<string> | string
  notAllowed?: Ref<string | string[] | undefined>
  /**
   * Callback to manually validate an input
   *
   * @returns a string or string[] with a validation message or a boolean false if invalid
   */
  validate?: (options: { value: string }) => void | string | string[] | false
}

// https://regex101.com/r/6tWHQ1/3
const regexPhone = '[0-9/+()\\s]+'

export type Props = {
  validationMessages?: Get<Options, 'validationMessages.value'>
  notAllowed?: Get<Options, 'notAllowed.value'>
}

export const useFormValidation = (options: Options) => {
  const hasFirstValidationHappendAlready = ref(false)
  const isInvalid = ref(false)
  const invalidMessage = ref<string>()

  const type = computed(() => {
    if (isRef(options.type)) return options.type.value
    else return options.type
  })

  const pattern = computed(() => {
    if (type.value === 'tel') {
      return regexPhone
    }

    return undefined
  })

  const validationMessagesNormalized = computed(() => {
    // Get default validtion messages for differen types
    let result: ValidationMessages = { ...validationMessagesBase }

    if (type.value === 'email') {
      result = { ...result, ...validationMessagesEmail }
    }

    if (type.value === 'number') {
      result = { ...result, ...validationMessagesNumber }
    }

    if (type.value === 'tel') {
      result = { ...result, ...validationMessagesPhone }
    }

    if (type.value === 'password') {
      result = { ...result, ...validationMessagesPassword }
    }

    if (type.value === 'select') {
      result = { ...result, ...validationMessagesSelect }
    }

    if (type.value === 'file') {
      result = { ...result, ...validationMessagesFile }
    }

    if (type.value === 'radio') {
      result = { ...result, ...validationMessagesRadio }
    }

    // Merge with prop validation message
    result = {
      ...result,
      ...options.validationMessages?.value,
    }

    const typedResultEntries = Object.entries(result) as Entries<typeof result>
    typedResultEntries.forEach(([key, value]) => {
      result[key] = replaceStringVariable(value)
    })

    return result
  })

  function replaceStringVariable(value: string | undefined) {
    if (!value) return value

    let result = value

    if (options.minlength?.value) {
      result = result.replaceAll('{minlength}', options.minlength.value.toString())
    }

    return result
  }

  function showInvalidMessage() {
    if (!options.elInput?.value) throw new Error('elInput is undefined')

    isInvalid.value = true
    hasFirstValidationHappendAlready.value = true

    /**
     * Check for custom validation messages to display
     */
    const invalidTypes: Array<keyof ValidityState> = []

    let key: keyof typeof options.elInput.value.validity
    for (key in options.elInput.value.validity) {
      const isInvalid = options.elInput.value.validity[key]

      if (isInvalid) {
        invalidTypes.push(key)
      }
    }

    const useCustomMessageForKey = invalidTypes.find((key) => {
      if (!validationMessagesNormalized.value) return

      const hasCustomMessage = Object.prototype.hasOwnProperty.call(
        validationMessagesNormalized.value,
        key
      )

      return hasCustomMessage
    })

    let validationMessage = options.elInput.value.validationMessage

    if (useCustomMessageForKey) {
      const customValidationMessage = validationMessagesNormalized.value?.[useCustomMessageForKey]

      if (customValidationMessage) {
        // setCustomValidity needs to be removed manually
        // this.$refs.input.setCustomValidity(validationMessage)
        validationMessage = customValidationMessage
      }
    }

    invalidMessage.value = validationMessage
  }

  function checkValidity() {
    if (!options.elInput?.value) throw new Error('elInput is undefined')

    const inputValue = options.elInput.value.value

    // Reset custom validity, will be checked again by methods below
    options.elInput.value.setCustomValidity('')

    // Manual validation callback
    checkForManualValidation({ value: inputValue })
    // Custom validations
    checkForNotAllowedInput({ value: inputValue })

    if (options.elInput.value.validity.valid) {
      isInvalid.value = false
      invalidMessage.value = undefined
    } else {
      if (!hasFirstValidationHappendAlready.value) return
      showInvalidMessage()
    }
  }

  /**
   * Automatically check validity on input
   */
  useEventListener(options.elInput, 'input', checkValidity)
  useEventListener(options.elInput, 'invalid', onInvalid)

  function maybeInit() {
    if (!options.elInput.value) return
    // Initially check the validity
    checkValidity()
  }

  watch(options.elInput, maybeInit, { immediate: true })

  /**
   * Check for not allowed inputs
   */
  const notAllowedNormalized = computed(() => {
    if (isNil(options.notAllowed)) return

    // Transform to array if it's a single string
    if (!Array.isArray(options.notAllowed.value)) {
      return [options.notAllowed.value]
    }

    return options.notAllowed.value
  })

  function checkForNotAllowedInput({ value }: { value: string }) {
    if (!options.elInput.value) return
    if (!notAllowedNormalized.value) return

    const valueIsInNotAllowed = notAllowedNormalized.value.includes(value)

    if (valueIsInNotAllowed) {
      options.elInput.value.setCustomValidity(
        validationMessagesNormalized.value.notAllowed ?? 'This value is not allowed'
      )
    }
  }

  /**
   * Manual validation callback
   */
  function checkForManualValidation({ value }: { value: string }) {
    if (!options.elInput.value) return

    if (!options.validate) return
    const result = options.validate({ value })
    let isValid = true
    let callbackInvalidMessage: string | string[] | undefined = undefined

    if (typeof result === 'boolean') isValid = result

    if (typeof result === 'string') {
      isValid = false
      callbackInvalidMessage = result
    }

    if (Array.isArray(result) && result.length > 0) {
      isValid = false
      // We can only set a string as the invalid message, therefore we just pick the first one:
      callbackInvalidMessage = result[0]
    }

    if (!isValid) {
      options.elInput.value.setCustomValidity(
        callbackInvalidMessage ??
          validationMessagesNormalized.value.callback ??
          'This value is invalid'
      )
    }
  }

  function onInvalid(event: Event) {
    event.preventDefault()
    showInvalidMessage()
  }

  return {
    isInvalid,
    invalidMessage,
    pattern,
    checkValidity,
    showInvalidMessage,
    onInvalid,
  }
}
