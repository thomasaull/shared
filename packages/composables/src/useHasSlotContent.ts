import { onUpdated, onMounted, useSlots, ref, type Slot, type VNode } from 'vue'

/**
 * @note This does not seem to work if the slot content is visible
 * depending on a computed property, the slot is not rendered, so the computed
 * prop is not evaluated, which would only be the case if the slot was rendered 😅
 * In case you encounter this problem, use `v-show` instead of `v-if`
 */
export function useHasSlotContent<
  const SlotConfigOrId extends readonly ({ id: string } | string)[],
>(slotConfigs: SlotConfigOrId) {
  type ExtractSlotId<T> = T extends { id: string } ? T['id'] : T
  type SlotId = ExtractSlotId<SlotConfigOrId[number]>

  // Normalize slotConfig
  type SlotConfigNormalized = { id: SlotId }
  const slotConfigsNormalized: SlotConfigNormalized[] = []

  slotConfigs.forEach((slotConfig) => {
    if (typeof slotConfig === 'string') {
      slotConfigsNormalized.push({ id: slotConfig as SlotId })
      return
    }

    slotConfigsNormalized.push(slotConfig as SlotConfigNormalized)
  })

  // Fill with initial values
  const initialValues: Record<SlotId, false> = {} as Record<SlotId, false>
  slotConfigsNormalized.forEach((slotConfig) => {
    initialValues[slotConfig.id] = false
  })
  const hasSlotContent = ref<Record<SlotId, boolean>>(initialValues)

  const slots = useSlots()

  function checkSlots() {
    slotConfigsNormalized.forEach((slotConfig) => {
      const slotHasContent = checkIfSlotHasContent(slots[slotConfig['id']], undefined)

      // @ts-expect-error difficult to type but should be alright anyway
      hasSlotContent.value[slotConfig['id']] = slotHasContent
    })
  }
  onUpdated(checkSlots)
  onMounted(checkSlots)

  return hasSlotContent
}

/**
 * Checks if a slot has content
 * @see https://github.com/vuejs/core/issues/4733#issuecomment-1024816095
 */
export function checkIfSlotHasContent(slot: Slot | undefined, slotProps = {}): boolean {
  if (!slot) return false

  return slot(slotProps).some((vnode: VNode) => {
    if (vnode.type === Comment) return false

    if (Array.isArray(vnode.children) && !vnode.children.length) return false

    if (vnode.type !== Text) return true

    if (typeof vnode.children === 'string') {
      if (vnode.children.trim() !== '') return true
    }

    return false
  })
}
