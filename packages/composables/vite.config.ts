import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv, mergeConfig } from 'vite'

import { createViteConfig } from '@thomasaull-shared/vite-config'

export const configOverrides = defineConfig((options) => {
  return {
    build: {
      lib: {
        entry: fileURLToPath(new URL('./src/index.ts', import.meta.url)),
        formats: ['es'],
      },

      rollupOptions: {
        external: ['vue'],
      },
    },
  }
})

export default defineConfig((configEnv) =>
  mergeConfig(
    createViteConfig(configEnv, {
      tsconfigPath: './tsconfig.json',
    }),

    configOverrides(configEnv)
  )
)
