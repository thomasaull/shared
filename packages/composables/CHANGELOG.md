# @thomasaull-shared/composables

## 2.1.0

### Minor Changes

- 83c9d69: 🚧 Use `none` as class name for a propModifier when the value is nil

## 2.0.3

### Patch Changes

- ff539ba: 🚧 (useXState) Add fixes

## 2.0.2

### Patch Changes

- be2d5d7: 🚧 Re-Export xstate and remove peer dependencies

## 2.0.1

### Patch Changes

- 527028e: 🐛 Improve types for `initialState` and `syncStateWith` options

## 2.0.0

### Major Changes

- ee940c0: ✨ Update xstate composable for xstate5

## 1.4.0

### Minor Changes

- 1b101ac: ✨ (useFormValidation) Improve init and error handling

  - Initialization now also works, when the input element is not mounted right away
  - The `validate` callback can return multiple error messages

## 1.3.0

### Minor Changes

- 4a79514: ✨ (useComponentClass) Add support for boolean prop modifiers

## 1.2.1

### Patch Changes

- b60426f: 🏷️ Add `checkbox` as a type to `useFormValidation`
- b5645e3: 🐛 (useFormValidation) Don't throw error when elInput is not found

## 1.2.0

### Minor Changes

- f45e505: ✨ Add support for custom validation callback to useFormValidation

## 1.1.5

### Patch Changes

- 73f0f8a: ✨ Add useHasSlotContent composable

## 1.1.4

### Patch Changes

- 49f01e5: 🚧 Add some small fixes to useFormValidation

## 1.1.3

### Patch Changes

- b7f9a50: ✨ Enable validation for not allowed input in useFormValidation

## 1.1.2

### Patch Changes

- 034848b: 🐛 (useComponentClass) className method should always return a string

## 1.1.1

### Patch Changes

- 9a649e3: 🏷️ Update `useComponentClass` with better types

## 1.1.0

### Minor Changes

- f7070dd: ✨ Add xstate composable

## 1.0.1

### Patch Changes

- 5ead9d1: ✨ Add `injectStrict` utility

## 1.0.0

### Major Changes

- 51f128e: ✨ Update useComponentClass composable

  - Breaking:
    - Rename: `propName` → `name`
    - Rename: `propValue` → `value`
  - Allow `undefined/null` as value

## 0.0.4

### Patch Changes

- ab80f15: 🐛 Fix package exports

## 0.0.3

### Patch Changes

- b6d0dd6: Add build step
- a173553: Add first draft of useFormValidation

## 0.0.2

### Patch Changes

- c830d96: ✨ Add `createPropModifier` method to `componentClass` composable

  It creates a modifier from a prop name and value in the form of MyComponent--propNameValue or MyComponent-element--propNameValue if element if provided

## 0.0.1

### Patch Changes

- cdbb123: 🚧 Fix bug on useComponentClass, add types package
- 3704792: Add package for vue composables and add useComponentClass
