/**
 * Automatically create icon imports from a provided directory
 */

import { glob } from 'glob'
import path from 'node:path'
import { camelCase } from 'lodash-es'

import { formatAndSave } from './formatAndSave'
import { pascalCase } from '../src/string'

type Options = {
  /**
   * Where the icon svg files are located
   * @default "./src/assets/icons"
   */
  srcDirectory?: string
  /**
   * Where to save the processed output
   * @note The directory will be deleted when the script runs
   * @default "./generated"
   */
  distDirectory?: string
}

async function run(options?: Options) {
  const srcDirectory = path.resolve(options?.srcDirectory ?? './src/assets/icons')
  const distDirectory = path.resolve(options?.distDirectory ?? './generated')
  const importBasePath = path.relative(distDirectory, srcDirectory)

  let svgFiles = await glob(`${srcDirectory}/*.svg`)
  // Sort alphabetically since for some reasiong the glob returns it in inverted direction
  svgFiles = svgFiles.sort()

  let outputImports = ''
  let outputExport = ''

  svgFiles.forEach((filePath) => {
    const fileName = path.basename(filePath)
    const extension = path.extname(fileName)
    const baseName = fileName.replace(extension, '')
    const moduleName = pascalCase(baseName)

    // https://regex101.com/r/fKK1GS/2
    const regexSuffix = /(?<nameWithoutSuffix>.*)\.(?<suffix>.*)\.svg/gm
    const { suffix, nameWithoutSuffix } = regexSuffix.exec(fileName)?.groups ?? {}

    let exportName = camelCase(baseName)

    // Update export name when suffix was found
    if (suffix && nameWithoutSuffix) {
      exportName = `'${camelCase(nameWithoutSuffix)}.${suffix}'`
    }

    outputImports += `import ${moduleName} from "${importBasePath}/${fileName}?component"
    `
    outputExport += `${exportName}: ${moduleName},
    `
  })

  const output = `
  /**
   * This file was automatically generated. Edits will be overwritten
   */

    ${outputImports}

    const icon = {
      ${outputExport}
    } as const

    export default icon
    export type Icon = keyof typeof icon
  `

  await formatAndSave(`${distDirectory}/icons.ts`, output)
}

export default run
