/**
 * Format contents with prettier and save file
 *
 */

import fs from 'fs/promises'
import prettier from 'prettier'
import { existsSync } from 'node:fs'

import { checkIfFilePath, maybeCreateDirectory } from './misc'

type OptionsBase = {
  /**
   * The prettier parser to use
   * @default typescript
   * @see https://prettier.io/docs/en/options.html#parser for available prettier parsers
   */
  parser?: 'typescript' | 'json' | 'markdown' | 'css'
}

/**
 * Format and save an output buffer
 */
type OptionsFormatAndSave = OptionsBase & {
  /**
   * Write the file only if the file contents have changed
   * @default false
   */
  onlyIfContentsChanged?: boolean
}

export async function formatAndSave(
  filePath: string,
  output: string,
  options: OptionsFormatAndSave = {}
) {
  const { parser = 'typescript', onlyIfContentsChanged = false }: OptionsFormatAndSave = options

  const prettierConfig = await prettier.resolveConfig(filePath)
  const formatted = await prettier.format(output, {
    ...prettierConfig,
    parser: parser,
  })

  checkIfFilePath(filePath)
  await maybeCreateDirectory(filePath)

  if (onlyIfContentsChanged && (await hasFileContentsChanged(formatted, filePath)) === false) {
    return
  }

  await fs.writeFile(filePath, formatted)
}

async function hasFileContentsChanged(output: string, filePath: string) {
  // File does not exist yet
  if (!existsSync(filePath)) return true

  const oldFileContents = await fs.readFile(filePath, {
    encoding: 'utf-8',
  })

  const hasChanged = oldFileContents !== output
  return hasChanged
}

/**
 * Read the contents of a file, format it and save the file
 */
export async function formatAndOverride(filePath: string, options: OptionsBase = {}) {
  const fileContents = await fs.readFile(filePath, {
    encoding: 'utf-8',
  })

  await formatAndSave(filePath, fileContents, options)
}
