/**
 * Wrapper around fs.copyFile which
 * - checks if the source file exists
 * - creates the target directory if it does not exist
 */

import fs from 'fs/promises'
import { existsSync } from 'node:fs'

import { checkIfFilePath, maybeCreateDirectory } from './misc'

async function copyFile(filePathFrom: string, filePathTo: string) {
  // Check if the src file exists
  if (!existsSync(filePathFrom)) {
    throw new Error(`copyFile: The source file "${filePathFrom} does not exists`)
  }

  checkIfFilePath(filePathTo)
  await maybeCreateDirectory(filePathTo)

  // Do the actual copy operation
  fs.copyFile(filePathFrom, filePathTo)
}

export default copyFile
