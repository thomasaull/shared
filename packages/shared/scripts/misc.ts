import path from 'node:path'
import fs from 'fs/promises'
import { existsSync } from 'node:fs'

export function checkIfFilePath(filePath: string) {
  const extension = path.extname(filePath)

  // Check if the filePath includes a file and extension
  if (!extension) {
    throw new Error(
      `No extension found, you should define your path with a file + extension, e.g. blupp.json. Provided path: ${filePath}`
    )
  }
}

/**
 * For a provided filePath create the directory if it doesn't exist yet
 */
export async function maybeCreateDirectory(
  /** Provide a full file path, not just the directory name */
  filePath: string
) {
  const directory = path.dirname(filePath)

  // Make sure directory exists
  if (!existsSync(directory)) {
    await fs.mkdir(directory, { recursive: true })
  }
}
