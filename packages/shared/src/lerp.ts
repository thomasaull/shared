export function lerp(start: number, stop: number, amount: number) {
  return start * (1 - amount) + stop * amount
}
