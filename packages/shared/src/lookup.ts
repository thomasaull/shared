type LookupTableBase = { [key: string | number]: string | number | unknown }
type LookupResult<LookupTable> = LookupTable[keyof LookupTable] | undefined

/**
 * Type-safe lookup in lookup tables
 */
export function lookup<Key extends string | number, LookupTable extends LookupTableBase>(
  key: Key | undefined | null,
  lookupTable: LookupTable
): LookupResult<LookupTable> | undefined {
  if (!key) return

  const keyExists = key in lookupTable

  if (!keyExists) {
    // console.trace(`key ${key} does not exist in lookupTable`)
    return undefined
  }

  return lookupTable[key]
}
