# @thomasaull-shared/scss

## 0.1.0

### Minor Changes

- 6775213: ✨ (cssVar) Add fallback option to `cssVar.use`

## 0.0.2

### Patch Changes

- b10b249: Add more helpers

  - mixin for svg icon default styles
  - functions for fluid values

## 0.0.1

### Patch Changes

- 8715cb5: Add scss package
