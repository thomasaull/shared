# @thomasaull-shared/storybook

## 0.0.5

### Patch Changes

- aa22b27: 🐛 Fix storybook helpers to work with generic vue components

## 0.0.4

### Patch Changes

- a3e748a: 🏷️ Fix types in storybook utilities

## 0.0.3

### Patch Changes

- 2d66a37: Update dependencies

## 0.0.2

### Patch Changes

- 719c3e6: 🚧 Remove peer dependencies

## 0.0.1

### Patch Changes

- a2bb08c: 🔧 Export createRenderFunction

## 1.0.0

### Major Changes

- 976f86d: Bump version to v1

## 0.0.1

### Patch Changes

- f3e5697: Trigger release of all packages
