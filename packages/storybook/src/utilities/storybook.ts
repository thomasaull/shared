import type { Meta, StoryObj, VueRenderer, StoryContext } from '@storybook/vue3'
import type { ArgsStoryFn } from '@storybook/types'
import { useArgs } from '@storybook/preview-api'
import type { Get } from 'type-fest'

type ArgTypeEvent = {
  action: string
}

type Options<Component> = {
  /** manual argTypes which get merged with automatically created ones */
  argTypes?: Meta<Component>['argTypes']
  parameters?: Meta<Component>['parameters']
  /** Additional actions not defined in `emits` which should show up in storybook */
  actions?: string[]
}

export function generateStoryMeta<Component>(component: Component, options?: Options<Component>) {
  /**
   * Add additional actions
   */
  const additionalActions: Record<string, ArgTypeEvent> = {}

  options?.actions?.forEach((actionName) => {
    additionalActions[transformEventName(actionName)] = { action: actionName }
  })

  /**
   * Handle events
   *
   * Use component.emits for this since the information is not available
   * Usually this is interfered by storybook itself, but somehow doesn't work yet with
   * the new vue-component-meta docgen
   */
  const defaultActions: Record<string, ArgTypeEvent> = {}

  if (
    component &&
    typeof component === 'object' &&
    'emits' in component &&
    Array.isArray(component.emits)
  ) {
    component.emits.forEach((eventName) => {
      if (eventName.startsWith('update:')) return
      defaultActions[transformEventName(eventName)] = { action: eventName }
    })
  }

  const argTypesFinal = {
    ...options?.argTypes,
    ...additionalActions,
    ...defaultActions,
  }

  return {
    component: component,
    argTypes: argTypesFinal,

    /**
     * Using poor-mans deepmerge to merge parameters
     *
     * If this becomes too tedious, a dedicated library for merging
     * deep objects is probably reasoneable
     */
    parameters: {
      ...options?.parameters,

      docs: {
        ...options?.parameters?.docs,

        source: {
          ...options?.parameters?.docs?.source,
          transform: transformCodePreview,
        },
      },
    },
  }
}

/**
 * Transform `eventName` to `onEventName`
 */
function transformEventName(eventName: string) {
  return `on${eventName.charAt(0).toUpperCase() + eventName.slice(1)}`
}

/**
 * Transforms the story code preview to a form where one
 * could copy paste the code-snippet into the codebase
 */
function transformCodePreview(code: string, storyContext: StoryContext) {
  let propsString = ''

  /**
   * Create prop-binds
   *
   * It checks the arg name against the component props, since the story
   * could be extended with additional args (for example, when a control is
   * used to render something in a slot)
   */
  if (storyContext.component && 'props' in storyContext.component) {
    const componentProps = Object.keys(storyContext.component.props ?? [])

    Object.entries(storyContext.args).forEach(([key, value]) => {
      if (!componentProps.includes(key)) return

      propsString += `${key}="${value}" `
    })
  }

  // Remove <template> tags if present
  code = code.replace('<template>', '')
  code = code.replace('</template>', '')

  // Replace `<component is="storyComponent"` with actual component name
  const componentName = storyContext.component?.__name
  if (componentName) {
    code = code.replace('<component is="storyComponent"', `<${componentName} `)
  }

  // Replace v-bind props
  // https://regex101.com/r/wluQ2w/1
  const regexProps = /v-bind=".*"/gm
  code = code.replace(regexProps, propsString)

  return code
}

type RenderFunction = ArgsStoryFn<VueRenderer, any>
// type RenderFunction = StoryObj['render']

function normalizeArgs(
  args: Parameters<RenderFunction>[0],
  context: Parameters<RenderFunction>[1]
) {
  Object.entries(args).forEach(([key, value]) => {
    if (!(key in context.argTypes)) return

    const argType = context.argTypes[key]
    const argControl = argType.control
    const hasControlType = !!(
      argControl &&
      typeof argControl === 'object' &&
      'type' in argControl &&
      argControl.type
    )

    if (!hasControlType) return
    if (!argControl.type) return

    const controlType = argControl.type

    if (controlType === 'date') {
      args[key] = normalizeDate(value)
    }
  })
}

function normalizeDate(value: unknown) {
  if (value instanceof Date) {
    return value
  }

  if (typeof value === 'number') {
    return new Date(value)
  }
}

// type Component = Parameters<RenderFunction>[1]['component']
// type Components = Record<any, Component>

// Use another way to get the type of render funtion components
type StoryObjRender = NonNullable<Get<StoryObj, 'render'>>
type StoryObjRenderReturn = ReturnType<StoryObjRender>
const typeHelper: StoryObjRenderReturn = {}
type Components = (typeof typeHelper)['components']
type Component = Get<Components, '0'>

type Args = Parameters<RenderFunction>[0]
type Context = Parameters<RenderFunction>[1]

type OptionsBase = {
  example?: number
}

type OptionsDifferentComponent = {
  component: Component
} & OptionsBase

type OptionsTemplate = {
  components: Components
  template: string
} & OptionsBase

export function generateStory(options?: OptionsDifferentComponent): any
export function generateStory(options?: OptionsTemplate): any
export function generateStory(
  options?: {
    component?: Component
    components?: Components
    template?: string
  } & OptionsBase
) {
  const render: StoryObj['render'] = (args, context) => {
    return createRenderFunction(args, context, options)
  }

  // const argTypes = generateArgTypes()

  return {
    render,
    // argTypes,
  }
}

/**
 * @deprecated Use `generateStory` instead!
 */
export function createRenderFunction(
  args: Args,
  context: Context,
  options?: {
    component?: Component
    components?: Components
    template?: string
  }
): any {
  type Story = StoryObj<typeof context.component>

  let updateArgs: ReturnType<typeof useArgs>[1] | undefined

  try {
    updateArgs = useArgs()[1]
  } catch {
    // silent error when useArgs method is not available
  }

  normalizeArgs(args, context)

  // Create args update
  Object.entries(context.argTypes).forEach(([key, value]) => {
    if (!value.table) return
    if (value.table.category !== 'events') return

    const isVModel = key.startsWith('update:')
    if (!isVModel) return

    const eventName = transformEventName(key)
    const argToUpdate = key.split(':')[1]

    if (!Object.keys(context.argTypes).includes(argToUpdate)) {
      return
    }

    args[eventName] = (newValue: any) => {
      // TODO: Manuall dispatch event to pass to storybook
      if (updateArgs) {
        updateArgs({ [argToUpdate]: newValue })
      } else {
        console.warn('updateArgs is undefined')
      }
    }
  })

  /**
   * Default case
   * No components and no template provided
   */
  let render: Story['render'] = {
    // @ts-expect-error Probably not working because of unknown component type
    components: { storyComponent: context.component },

    setup() {
      return { args }
    },

    template: '<component is="storyComponent" v-bind="args" />',
  }

  /**
   * Only a single component provided
   * e.g. for story components
   */
  if (options?.component) {
    render = {
      // @ts-expect-error Probably not working because of unknown component type
      components: { storyComponent: options.component },

      setup() {
        return { args }
      },

      template: '<component is="storyComponent" v-bind="args" />',
    }
  }

  /**
   * Components and template provided
   */
  if (options?.components && options?.template) {
    render = {
      // @ts-expect-error Probably not working because of unknown component type
      components: options.components,

      setup() {
        return { args }
      },

      template: options.template,
    }
  }

  // Don't know how to type this
  return render
}
