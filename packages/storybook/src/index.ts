import type { ComponentProps } from 'vue-component-type-helpers'
import type { Meta } from '@storybook/vue3'

export type { Meta, StoryObj } from '@storybook/vue3'
export { generateStoryMeta, generateStory, createRenderFunction } from './utilities/storybook'
export * from '@storybook/blocks'
export type { ComponentProps }

// https://github.com/storybookjs/storybook/issues/24238
export type GenericMeta<C> = Omit<Meta<ComponentProps<C>>, 'component'> & {
  component: Record<keyof C, unknown>
}
