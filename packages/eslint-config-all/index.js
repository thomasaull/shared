/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:storybook/recommended',
    'plugin:vue/vue3-recommended',
    'eslint:recommended',
    '@vue/eslint-config-typescript/recommended',
    '@vue/eslint-config-prettier',
    'plugin:storybook/recommended',
  ],

  parserOptions: {
    ecmaVersion: 'latest',
  },

  rules: {
    /**
     * Allow unused vars, but show warning
     * @see https://typescript-eslint.io/rules/no-unused-vars/
     */
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'warn',

    /**
     * Allow any with a warning
     * @see https://typescript-eslint.io/rules/no-explicit-any/
     */
    '@typescript-eslint/no-explicit-any': 'warn',

    /**
     * Enforce hypen
     * @see https://eslint.vuejs.org/rules/v-on-event-hyphenation.html
     */
    'vue/v-on-event-hyphenation': ['error', 'always', { autofix: true }],

    /**
     * Maintain consistent order of top-level blocks in single file components
     * @see https://eslint.vuejs.org/rules/component-tags-order.html
     */
    'vue/component-tags-order': [
      'error',
      {
        order: ['template', 'script:not([setup])', 'script[setup]', 'style'],
      },
    ],

    /**
     * Allow @ts-ignore when a comment with a description is added
     */
    '@typescript-eslint/ban-ts-comment': [
      'warn',
      {
        'ts-expect-error': 'allow-with-description',
        'ts-ignore': 'allow-with-description',
        // 'ts-nocheck': true,
        // 'ts-check': false,
        minimumDescriptionLength: 3,
      },
    ],

    /**
     * Turn off require default for optional props
     * @see https://eslint.vuejs.org/rules/require-default-prop.html
     */
    'vue/require-default-prop': 'off',
  },
}
