# @thomasaull-shared/storybook-config

## 1.0.0

### Major Changes

- 976f86d: Bump version to v1

## 0.0.1

### Patch Changes

- f3e5697: Trigger release of all packages
