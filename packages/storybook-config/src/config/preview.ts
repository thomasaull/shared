import { type Preview } from '@storybook/vue3'
export { setup, type Preview, type Decorator } from '@storybook/vue3'

export const defaultConfig: Preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
}
