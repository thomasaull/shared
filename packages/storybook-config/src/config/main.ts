import type { StorybookConfig } from '@storybook/vue3-vite'
export type { StorybookConfig } from '@storybook/vue3-vite'
import path from 'node:path'
import remarkGfm from 'remark-gfm'

export function createStoriesConfig(options: { storiesPaths: string[] }) {
  const stories: string[] = []

  // Add file endings to stories paths
  options.storiesPaths.forEach((storiesPath) => {
    stories.push(`${storiesPath}/**/*.stories.@(js|jsx|mjs|ts|tsx)`)
    stories.push(`${storiesPath}/**/*.mdx`)
  })

  return stories
}

export function createConfig(options: { storiesPaths: string[] }) {
  const config: StorybookConfig = {
    ...defaultConfig,
    stories: createStoriesConfig({ storiesPaths: options.storiesPaths }),
  }

  return config
}

type StorybookBaseConfig = Omit<StorybookConfig, 'stories'>

export const defaultConfig: StorybookBaseConfig = {
  addons: [
    '@storybook/addon-links',
    {
      name: '@storybook/addon-essentials',
      options: {
        docs: false,
      },
    },
    {
      name: '@storybook/addon-docs',
      options: {
        mdxPluginOptions: {
          mdxCompileOptions: {
            remarkPlugins: [remarkGfm],
          },
        },
      },
    },
    '@storybook/addon-interactions',
  ],

  framework: {
    name: '@storybook/vue3-vite',
    options: {
      docgen: 'vue-component-meta',
    },
  },

  docs: {
    autodocs: false,
  },
}
