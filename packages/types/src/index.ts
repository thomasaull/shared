/** Alias type for value that also can be null or undefined */
export type Nilable<T> = T | null | undefined

/** Alias type for value that also can be null */
export type Nullable<T> = T | null

/**
 * Makes only some parts of an interface optional
 * @see https://pawelgrzybek.com/make-the-typescript-interface-partially-optional-required/
 */
export type PartiallyOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>

/**
 * Makes only some parts of an interface required
 * @see https://pawelgrzybek.com/make-the-typescript-interface-partially-optional-required/
 */
export type PartiallyRequired<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>
